import * as bootstrap from 'bootstrap/dist/js/bootstrap'

let nodeApi = ''

export function setNodeApi(api) {
    nodeApi = api
}

export default class GifImage {

    displayDiv = null
    myModal = null

    static get toolbox() {
        return {
            title: 'GIF',
            icon: '<svg width="17" height="15" viewBox="0 0 336 276" xmlns="http://www.w3.org/2000/svg"><path d="M291 150V79c0-19-15-34-34-34H79c-19 0-34 15-34 34v42l67-44 81 72 56-29 42 30zm0 52l-43-30-56 30-81-67-66 39v23c0 19 15 34 34 34h178c17 0 31-13 34-29zM79 0h178c44 0 79 35 79 79v118c0 44-35 79-79 79H79c-44 0-79-35-79-79V79C0 35 35 0 79 0z"/></svg>'
        };
    }

    static get pasteConfig() {
        return {
            patterns: {
                image: /https?:\/\/\S+\.(gif)$/i
            }
        };
    }


    render() {
        const element = document.createElement('div')
        this.displayDiv = element
        this.myModal = this.buildAndShowModal(element);
        return element;
    }

    __createImage(src) {
        const img = document.createElement('img')
        img.style.width = '200px'
        img.style.height = '200px'
        img.src = src
        this.displayDiv.append(img)
        this.myModal.hide()
    }

    onPaste(event) {
        switch (event.type) {
            case 'pattern':
                const src = event.detail.data
                this.__createImage(src)
                break;
        }
    }

    save(blockContent) {
        const elements = [];
        for (let child of blockContent.childNodes) {
            elements.push(child.src)
        }
        return elements
    }

    buildAndShowModal(imgEl) {
        if (document.getElementById('modal_gif')) {
            document.getElementById('modal_gif').remove()
        }
        const div = document.createElement('div')
        div.innerHTML = `
<div class="modal" data-bs-delay='{"show":0,"hide":150}' id="modal_gif" aria-labelledby="modal" aria-hidden="true" tabindex="-1">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Choose GIF</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body" style="display: flex;flex-wrap: wrap;justify-content: center">
      <input name="page" hidden value="0">
      <input name="search" type="text" placeholder="Enter your search here" class="mb-2" style="flex: 1 0 100%">
      <div id="gifs" style="flex: 1 0 100%;display: flex;flex-wrap: wrap;height: 40rem;overflow: auto"></div>
      <nav aria-label="Page navigation example" >
        <ul class="pagination">
            <li class="page-item disabled" id="prev">
            <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
            </li>
            <li class="page-item  disabled" id="next">
              <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Next</a>
            </li>

        </ul>
        </nav>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button id="save_button" type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
        `
        const myModalDiv = div.querySelector(`#modal_gif`);
        const searchInput = div.querySelector(`input[name='search']`);
        const myModal = new bootstrap.Modal(myModalDiv, {})
        const gifsDiv = myModalDiv.querySelector('#gifs')
        const prev = myModalDiv.querySelector('#prev')
        const next = myModalDiv.querySelector('#next')
        const page = myModalDiv.querySelector(`input[name='page']`)

        // prepare pagination
        prev.addEventListener('click', this.goPrevPage(page, gifsDiv, searchInput))
        next.addEventListener('click', this.goNextPage(page, gifsDiv, searchInput, prev))

        // on search key enter
        searchInput.addEventListener('change', this.getGifs(gifsDiv, next, page, true));

        // on save show the selected gif
        myModalDiv.querySelector('#save_button').addEventListener('click', function (event) {
            // get the selected gif img and get src and use it for display
            for (let img of gifsDiv.querySelectorAll('.selected_gif')) {
                const temp = document.createElement('img');
                temp.src = img.dataset.original
                temp.style.width = '200px'
                temp.style.height = '200px'
                imgEl.append(temp);
            }
            myModal.hide()
        })
        // remove unused html
        myModalDiv.addEventListener('hidden.bs.modal', () => {
            myModalDiv.remove()
        })
        myModal.show()
        return myModal
    }

    /**
     * Handle Next page click
     * @param page page counter
     * @param gifsDiv where to place gifs
     * @param searchInput seach field
     * @param prev previous button
     * @returns {(function(*): void)|*}
     */
    goNextPage(page, gifsDiv, searchInput, prev) {
        const getGifs = this.getGifs
        return function (event) {
            if (!event.target.classList.contains('disabled')) {
                page.value = parseInt(page.value) + 1
                getGifs(gifsDiv, event.target, page)({target: searchInput})
                prev.classList.remove('disabled')
            }
        }
    }

    /**
     * Handle Next page click
     * @param page page counter
     * @param gifsDiv where to place gifs
     * @param searchInput seach field
     * @returns {(function(*): void)|*}
     */
    goPrevPage(page, gifsDiv, searchInput) {
        const getGifs = this.getGifs
        return function (event) {
            if (!event.target.classList.contains('disabled')) {
                page.value = parseInt(page.value) - 1
                getGifs(gifsDiv, event.target, page)({target: searchInput})
                if (page.value == 0) {
                    event.target.classList.add('disabled')
                }
            }
        }
    }

    /**
     * Handle getting the gifs from search or from pagination buttons
     * @param gifsDiv div to display gifs
     * @param next next page button
     * @param page page field
     * @param isSearch boolean to use pagination or event
     * @returns {(function(*): void)|*}
     */
    getGifs(gifsDiv, next, page, isSearch = false) {
        return function (event) {
            // Call search api
            gifsDiv.innerHTML = '';
            if (isSearch)
                page.value = 0
            axios({
                method: 'get',
                url: `${nodeApi}?q=${event.target.value}&page=${page.value}`
            }).then(response => {
                for (let gif of response.data.data) {
                    const gifEl = document.createElement('img');
                    gifEl.src = gif.images.downsized_still.url
                    gifEl.dataset.original = gif.images.original.url
                    gifEl.style.flex = '1'
                    gifEl.style.width = '200px'
                    gifEl.style.height = '200px'
                    gifEl.alt = 'gif'
                    gifEl.addEventListener('click', function (event) {
                        // add new class to selected
                        if (event.target.classList.contains('selected_gif')) {
                            event.target.classList.remove('selected_gif')
                            event.target.style.border = '0'
                        } else {
                            event.target.classList.add('selected_gif')
                            event.target.style.border = '1.3px solid red'

                        }
                    })
                    gifsDiv.append(gifEl)
                }
                // set pages based on
                if (response.data.numberOfPages > 0 && page.value < response.data.numberOfPages) {
                    next.classList.remove('disabled')
                } else {
                    next.classList.add('disabled')
                }

            })
        }
    }
}
