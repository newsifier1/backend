<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class ArticleController extends Controller
{
    //

    public function __construct()
    {
    }

    public function create(){
        return Inertia::render('Articles/Create', [
            'nodeApi' => config('nodeApi.base_url')
        ]);
    }

    public function store(Request $request){
        $data = $request->validate([
            'title' => 'required',
            'content' => 'required'
        ]);

        Article::create($data);

        return Redirect::back();
    }

    public function show(Request $request, Article $article){
        return Inertia::render('Articles/Show', [
            'article' => $article
        ]);
    }
}
