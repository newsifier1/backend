# Structure
___
## Newsifier
A project to manage articles 
- Show specific article by slug
- Add new article only for registered users

## Project run
- create new database
- copy and change name of the .env file and set your APP_URL and APP_NAME
- generate new key for the .env\
`php artisan key:generate`
- migrate and seed the database\
`php artisan migrate --seed`
- run npm install
- serve the project and you can test the api's
`php artisan serve`

## Nodejs Server
- you need to clone and run the nodejs server
`npm install`
`node index`

## Why nodejs server is not inside laravel
- That's because it will be confusing having it with all it's structure inside a laravel project.

# Functions
___
## Laravel 
- Authentication system is made by Breeze scaffolding vue.js version to build the least amount of views
- There is 3 views:\
1- Dashboard: /dashboard ::Get\
2- Create Article: /articles/create ::Get\
3- Show Article for public users: /articles/{slug} ::Get\ 
- And one route for posting the new Article: /articles ::Post
- Editor.js plugin is reusable and all the API call inside it and html is auto generated
- We can upgrade it to become a whole new package

## Nodejs
- There is no authentication system nor Auth2 system because we are using public API inside nodejs
- There is error logs and response
- Used Express.js with MVC pattern
